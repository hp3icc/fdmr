# FDMR+

<img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/Screenshot_156.jpg" width="550" height="450"><img src="https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/main/screenshot.png" width="550" height="450">


ADN Systems DMR Peer Server is a fork from FreeDMR by G7RZU hacknix
Launched on April 21, 2024, by a visionary group of 20 international amateur radio enthusiasts, ADN Systems operates on an Open Bridge Protocol (OBP), fostering a decentralized network devoid of hierarchical structures.

This shell, install ADN Peer Server,with 2 option Dashboard for select: FDMR-Monitor by OA4DOA Christian and FDMR-Monitor by CS8ABG Bruno , Both Dashboard version Self-Service

#

Shell easy auto install ADN Peer Server version Self-Service without Docker latest release, with Dashboard, template mods by WP3JM James & N6DOZ Rudy, Self-Service mods with Dial-TG by IU2NAF Diego and menu by HP3ICC.

# Compatibility

* You can use this script on raspberry , linux pc , server , virtual machine or vps with debian 11 x86 or x64


* This script contains binaries created by different developers , many designed to be used on debian 11 or higher , bad news for ubuntu users , some of the included applications may only work correctly on debian 11

   <img src="https://adn.systems/wp-content/uploads/2024/04/logo1.png" width="250" height="150">
 

* Important, this script is preconfigured to be used in the ADN Systems server mesh

* Very Important, for greater stability in the file downloads that the server and dashboard perform every 24 hours, this script disables IPv6. If you require IPv6 on your host computer, you must reactivate it at your own risk.


# Pre-Requirements

need have curl and sudo installed

#

# Install

into your ssh terminal copy and paste the following link :

    apt update

    apt install curl sudo -y

    sudo su
    
    bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/fdmr/-/raw/main/easy-fdmr.sh)"
             
             
 #            
  
 # Menu
 
 ![alt text](https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-SERVER-Install/main/IMG_1941.jpg)
 
  At the end of the installation your freedmr Peer server will be installed and working, a menu will be displayed that will make it easier for you to edit, restart or update your server and dashboard to future versions.
  
  to use the options menu, just type "menu-adn" without the quotes in your ssh terminal or console.
  
 #
 
 # Self-Service
 
 ![alt text](https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-Docker/main/self-service-docker.jpg)
 
 The self-service feature, added by fellow OA4DOA Christian, allows each user to add or remove one or more static tgs from the ease of a graphical environment from the server's Dashboard. 
 
 Thanks to colleague IU2NAF Diego and the Italia team, they compatibility and option to customize the language of the announcement voice or use CW.
 
 # Self-Service database

 The Self-Service function uses a local database, this database does not store private information, it only stores the callsign, id and list of static tgs, created by the same user, the password is the callsign that the hotspot has and The password is decided by the user from his hotspot in the options line, without sending a previous request, filling out a ticket, sending an email or asking someone else for authorization. The user can configure the same password for all their hotspots, repeaters or connections, or they can configure an independent password for each connected equipment. They can only use Self-Service if they have previously configured their password in the options line and their equipment or application. is connected to the server
 
 #
 
 # Self-Service username and password
 
 The user will always be indicative that he has his hotspot or application connected to the server. 

the password is chosen by the user and placed in the options line as follows: " PASS=password_of user_selfservice"

Password example " abc123 " :

Options=PASS=abc123
 
<img src="https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-Docker/main/pistar.jpg" width="250" height="280"> <img src="https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-Docker/main/droidstar.jpg" width="200" height="280"> <img src="https://raw.githubusercontent.com/hp3icc/Easy-FreeDMR-Docker/main/mmdvm.jpg" width="250" height="280">
 
 The password must contain at least 6 characters between letters and numbers, you cannot use your callsign as a password.
 
 
 #
 
 # How to Update FDMR+

In case of bugs, changes, updates or new versions, you can use the update option from the fdmr+ menu:

To facilitate your update, copy all your obp in the opb list menu option, when when your server update is finished, all your obp will be automatically added to the configuration file and your server will be operational in minutes

* Select menu update

   <img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/IMG_5900.jpg" width="250" height="280">

* Select update ADN Server

   <img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/IMG_5899.jpg" width="250" height="280"> 

* Select OBP List, and verify that all your obp are listed correctly

   <img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/IMG_5909.jpg" width="250" height="280">

   <img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/IMG_5908.jpg" width="250" height="280">

* Select iniciar actualizacion / strart update
  
   <img src="https://gitlab.com/hp3icc/fdmr/-/raw/main/img/IMG_5901.jpg" width="250" height="280">

 #
  
 # Location files config :
 
  * ADN DMR Peer Server:  
   
   /opt/ADN-DMR-Peer-Server/config/adn.cfg
   
  * FDMR-Monitor OA4DOA: 
   
   /opt/FDMR-Monitor/fdmr-mon.cfg 

  * FDMR-Monitor2 CS8ABG: 
   
   /opt/FDMR-Monitor2/fdmr-mon.cfg 
   
  #
  
  # Systemctl Services :
  
  * ADN DMR server: 
   
   adn-server.service
   
  * ADN server Proxy: 
   
   proxy.service
   
  * ADN server Parrot: 
   
   adn-parrot.service
  
  * Web Server
  
   nginx.service
  
  * FDMR-Monitor OA4DOA: 
   
   fdmr_mon.service

  * FDMR-Monitor2 CS8ABG: 
   
   fdmr_mon2.service
  
 #
  
 # Dashboard Files
 
 * FDMR-Monitor by OA4DOA

 /var/www/fdmr/

* FDMR-Monitor2 by CS8ABG

 /var/www/fdmr2/

#

* Support
    <p><a title="ADN Systems - User Group" href="https://t.me/ADN_Systems" target="_blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/15566648/telegram_icon.png" alt="" width="70" height="70" /></a></p>
Click on the Telegram icon and join the ADN Systems family

Contact us in our Telegram group, and join a truly free network for amateur radio use, based on 3 pillars, equality, respect and responsibility.

 If you have the basic knowledge of Linux to install, configure and manage a DMR server, you can integrate your server to the ever-growing network of mesh servers of Amateur radio digital network systems.

#


 # Credits :
 
Special thanks to colleagues: CE5RPY Rodrigo, CS8ABG Bruno, OA4DOA Christian, G7RZU hacknix, for their contributions to the content of this scrip.

#

 # Sources :
 
 * https://github.com/Amateur-Digital-Network/ADN-DMR-Peer-Server
 
 * https://gitlab.hacknix.net/hacknix/FreeDMR

 * https://github.com/CS8ABG/FDMR-Monitor/tree/Self_Service
 
 * https://github.com/yuvelq/FDMR-Monitor/tree/Self_Service
 
 * https://www.daniloaz.com/es/como-crear-un-usuario-en-mysql-mariadb-y-concederle-permisos-para-una-base-de-datos-desde-la-linea-de-comandos/
 
 * https://www.tecmint.com/install-lamp-debian-11/

 * https://styde.net/crear-una-base-de-datos-en-mysql-mariadb/

