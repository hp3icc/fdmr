#!/bin/bash
# Nombre del script
SCRIPT_NAME="FreeDMR-Bridge.sh"

# Registra el inicio en /opt/curl.txt
echo "Inicio: $SCRIPT_NAME" >> /opt/curl.txt
cd /opt/
if [ -f "/opt/FreeDMR-Bridge/config/FreeDMR.cfg" ];
then
   variable=$(grep "SERVER_ID:" /opt/FreeDMR-Bridge/config/FreeDMR.cfg | grep -Eo '[0-9]{1,9}')
 else
  echo "id not found"

fi
if [ -z "$variable" ]
then variable=00000

fi
if [ -d "/opt/FreeDMR-Bridge" ]
then
  sudo rm -r /opt/FreeDMR-Bridge
fi
if [ ! -d "/var/log/FreeDMR-B" ]
then
  mkdir -p /var/log/FreeDMR-B
fi
git clone https://gitlab.hacknix.net/hacknix/FreeDMR.git /opt/FreeDMR-Bridge
cd FreeDMR-Bridge
sudo git checkout v1.3.19
mkdir /opt/FreeDMR-Bridge/config
sudo chmod +x /opt/FreeDMR-Bridge/*
cp /opt/FreeDMR-Bridge/rules_SAMPLE.py /opt/FreeDMR-Bridge/config/rules.py
if [ "$(cat /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg | grep 'KEYS_FILE')" != "" ]; then
sudo sed -i 's/KEYS_FILE:.*/KEYS_FILE: keys.json/' /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg 
else
sudo sed '45 a KEYS_FILE: keys.json' -i /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg 
fi
sed -i "s|SUB_MAP_FILE:.*|SUB_MAP_FILE: sub_map.pkl|g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s/ANNOUNCEMENT_LANGUAGE:.*/ANNOUNCEMENT_LANGUAGE: es_ES/g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s/54000/57000/g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i 's/file-timed/console-timed/' /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i 's/INFO/DEBUG/' /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i 's/freedmr.log/\/var\/log\/FreeDMR-B\/FreeDMR.log/' /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s/SERVER_ID:.*/SERVER_ID: $variable/g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s/4321/4382/g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i '220s/20/50/' /opt/FreeDMR-Bridge/hotspot_proxy_v2.py
#sed -i "s|VALIDATE_SERVER_IDS:.*|VALIDATE_SERVER_IDS: False|g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
#
sed -i "s|SINGLE_MODE:.*|SINGLE_MODE: False|g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|VOICE_IDENT:.*|VOICE_IDENT: False|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|PEER_FILE:.*|PEER_FILE: peer_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|SUBSCRIBER_FILE:.*|SUBSCRIBER_FILE: subscriber_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|TGID_FILE:.*|TGID_FILE: talkgroup_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|PEER_URL:.*|PEER_URL: https://adn.systems/files/peer_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|SUBSCRIBER_URL:.*|SUBSCRIBER_URL: https://adn.systems/files/subscriber_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|TGID_URL:.*|TGID_URL: https://adn.systems/files/talkgroup_ids.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|STALE_TIME:.*|STALE_TIME: 1|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|SUB_MAP_FILE:.*|SUB_MAP_FILE: sub_map.pkl|g" /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|SERVER_ID_FILE:.*|SERVER_ID_FILE: server_ids.tsv|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|CHECKSUM_URL:.*|CHECKSUM_URL: https://adn.systems/files/file_checksums.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|CHECKSUM_FILE:.*|CHECKSUM_FILE: file_checksums.json|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
sed -i "s|SERVER_ID_URL:.*|SERVER_ID_URL: https://adn.systems/files/server_ids.tsv|g"  /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg
#
cp /opt/FreeDMR-Bridge/FreeDMR-SAMPLE.cfg /opt/FreeDMR-Bridge/config/FreeDMR.cfg
sudo cat >  /opt/FreeDMR-Bridge/proxy.cfg <<- "EOF"
[PROXY]
MASTER = 127.0.0.1
LISTENPORT = 62031
# Leave blank for IPv4, :: = all IPv4 and IPv6 (Dual Stack)
LISTENIP =
DESTPORTSTART = 57000
DESTPORTEND = 57099
TIMEOUT = 30
STATS = False
DEBUG = False
CLIENTINFO = False
BLACKLIST = [1234567,1231237,123123701]
#e.g. {10.0.0.1: 0, 10.0.0.2: 0}
IPBLACKLIST = {}


EOF
sudo cat > /lib/systemd/system/freedmr-bridge.service <<- "EOF"
[Unit]
Description=FreeDmr-Bridge
After=multi-user.target

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
ExecStart=/usr/bin/python3 /opt/FreeDMR-Bridge/bridge.py -c /opt/FreeDMR-Bridge/config/FreeDMR.cfg -r /opt/FreeDMR-Bridge/config/rules.py
Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
###
systemctl daemon-reload

##############################################################################################
#FDMR-Monitor
if [ ! -d "/var/www" ]
then
  mkdir -p /var/www
fi
if [ -d "/var/www/fdmr-b" ]
then
  rm -r /var/www/fdmr-b
fi
if [ -d "/opt/FDMR-Monitor-B" ]
then
  rm -r /opt/FDMR-Monitor-B
fi
mkdir /var/www/fdmr-b
cd /opt
#sudo git clone https://github.com/yuvelq/FDMR-Monitor.git /opt/FDMR-Monitor-B
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor.git /opt/FDMR-Monitor-B
cd /opt/FDMR-Monitor-B
sudo chmod +x install.sh
chmod 755 /opt/FDMR-Monitor-B/data
sed -i "s/4321/4382/g"  /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg
find /opt/FDMR-Monitor-B -type f -exec sed -i 's/9000/9200/g' {} +
sed -i 's/RELOAD_TIME =.*/RELOAD_TIME = 1/' /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg
sed -i 's/FREQUENCY =.*/FREQUENCY = 1/' /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg
sudo chmod 644 /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg
sed '30 a <!--' -i /opt/FDMR-Monitor-B/html/sysinfo.php
sed '32 a -->' -i /opt/FDMR-Monitor-B/html/sysinfo.php 
####
sed -i "s/www\/html/www\/fdmr-b/g" /opt/FDMR-Monitor-B/html/*.*
sed -i "s/www\/html/www\/fdmr-b/g" /opt/FDMR-Monitor-B/sysinfo/*.*
sed -i "s/1100/1200/g" /opt/FDMR-Monitor-B/html/*.*
sed -i "s/1100/1200/g" /opt/FDMR-Monitor-B/html/css/*.*
sed -i "s/1100/1200/g" /opt/FDMR-Monitor-B/templates/*.*
sed -i 's/b1eee9/3bb43d/' /opt/FDMR-Monitor-B/html/css/*.*
####
sed -i 's/localhost_2-day.png/localhost_1-day.png/' /opt/FDMR-Monitor-B/html/sysinfo.php
sed -i "s/HBMonv2/FDMR-Monitor/g"  /opt/FDMR-Monitor-B/sysinfo/*.sh
sudo chmod +x /opt/FDMR-Monitor-B/sysinfo/cpu.sh
sudo chmod +x /opt/FDMR-Monitor-B/sysinfo/graph.sh
sudo chmod +x /opt/FDMR-Monitor-B/sysinfo/rrd-db.sh

#sudo rm /opt/FDMR-Monitor-B/install.sh
################
apps=("rrdtool")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done
sed -i "s/www\/html/www\/fdmr-b/g" /opt/FDMR-Monitor-B/html/*.*
sed -i "s/www\/html/www\/fdmr-b/g" /opt/FDMR-Monitor-B/sysinfo/*.*
# Install the required support programs
#sudo pip install -U -r requirements.txt
cp /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg /opt/FDMR-Monitor-B/fdmr-mon.cfg
###############################
sed -i "s|PEER_URL =.*|PEER_URL = https://database.radioid.net/static/rptrs.json|g" /opt/FDMR-Monitor-B/fdmr-mon.cfg
#sed -i "s/PEER_URL =.*/PEER_URL = https:\/\/freedmr-lh.gb7fr.org.uk\/json\/peer_ids.json/g" /opt/FDMR-Monitor-B/fdmr-mon.cfg
sed -i "s/SUBSCRIBER_URL =.*/SUBSCRIBER_URL = http:\/\/datafiles.ddns.net:8888\/user.json/g" /opt/FDMR-Monitor-B/fdmr-mon.cfg
sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" /opt/FDMR-Monitor-B/fdmr-mon.cfg
sed -i "s/TGID_URL =.*/TGID_URL = http:\/\/datafiles.ddns.net:8888\/talkgroup_ids.json/g" /opt/FDMR-Monitor-B/fdmr-mon.cfg
#################

#wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1B-M7QNdf1gLVzbTn-Fi5GVPy6GTXcxJ-' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1B-M7QNdf1gLVzbTn-Fi5GVPy6GTXcxJ-" -O /opt/FDMR-Monitor-B/html/favicon.ico && rm -rf /tmp/cookies.txt &&
sed '6 a <link rel="shortcut icon" href="/favicon.ico" />' -i /opt/FDMR-Monitor-B/html/index.php
#wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1evvxLOh8uxKYYLoV0aORjDhFeLF42_S_' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1evvxLOh8uxKYYLoV0aORjDhFeLF42_S_" -O /opt/FDMR-Monitor-B/html/img/logo.png && rm -rf /tmp/cookies.txt &&
#
sudo cat > /opt/FDMR-Monitor-B/html/buttons.php <<- "EOF"
<!-- HBMonitor buttons HTML code -->
<a class="button" href="index.php">Home</a>
&nbsp;
<div class="dropdown">
  <button class="dropbtn">Links</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="linkedsys.php">Linked Systems</a>

<a class="button" href="statictg.php">Static TG</a>

<a class="button" href="opb.php">OpenBridge</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">Local Server</button>
  <div class="dropdown-content">

<a class="button" href="moni.php">&nbsp;Monitor&nbsp;</a>

<a class="button" href="sysinfo.php">&nbsp;System Info&nbsp;</a>

<a class="button" href="log.php">&nbsp;Lastheard&nbsp;</a>

<a class="button" href="tgcount.php">&nbsp;TG Count&nbsp;</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">FreeDMR</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="https://freedmr-lh.gb7fr.org.uk/?limit=100&recent=1"target="_blank">&nbsp;Global Info FreeDMR&nbsp;</a>
<a class="button" href="http://www.freedmr.uk/index.php/freedmr-servers/"target="_blank">&nbsp;Info Server&nbsp;</a>
<a class="button" href="http://www.freedmr.uk/index.php/world-wide-talk-groups/"target="_blank">&nbsp;World Wide Talk Groups&nbsp;</a>
<a class="button" href="http://www.freedmr.uk/freedmr/option-calculator-b.php"target="_blank">&nbsp;Static TG Calculator&nbsp;</a>
&nbsp;
</div>
</div>

<!--
<a class="button" href="bridges.php">Bridges</a>
-->
<!-- Example of buttons dropdown HTML code -->
<!--
<div class="dropdown">
  <button class="dropbtn">Admin Area</button>
  <div class="dropdown-content">
    <a href="masters.php">Master&Peer</a>
    <a href="opb.php">OpenBridge</a>
    <a href="moni.php">Monitor</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Reflectors</button>
  <div class="dropdown-content">
    <a target='_blank' href="#">YSF Reflector</a>
    <a target='_blank' href="#">XLX950</a>
  </div>
</div>
-->

EOF

#
sudo cat > /opt/FDMR-Monitor-B/templates/main_table.html  <<- "EOF"

<fieldset class="big">
<legend><b><font color="#000">&nbsp;.: Server Activity :.&nbsp;</font></b></legend>
{% if _table['MASTERS']|length >0 %}
 <table style="table-layout:fixed;width:1100px; font: 10pt arial, sans-serif;margin-top:5px;margin-bottom:5px;" width=1100px>
    <tr style="background-color:#265b8a;" "height:30px;font: 10pt arial, sans-serif;{{ themec }}">
        <th>Systems M&P</th>
        <th>Source</th>
        <th>Destination</th>        
    </tr>
    {% for _master in _table['MASTERS'] %}    
    {% for _client, _cdata in _table['MASTERS'][_master]['PEERS'].items() %}
    {% if _cdata[1]['TS'] == True or _cdata[2]['TS'] == True %}
    <tr style="background-color:#a1dcb5;">
        {% if _cdata[1]['TRX'] == "RX" %}
        <td style="font-weight:bold; padding-left: 20px; text-align:center;color:#464646;">M: {{_master}} </td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#0d1a81;">{{ _cdata[1]['SUB']|safe }} [<span style="align-items: center;justify-content:center;font-size: 8pt;font-weight:600;color:brown;">TS {{ 1 if _cdata[1]['TS'] == True else 2 }}</span>]</td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#b5651d;">{{ _cdata[1]['DEST']|safe }}</td>
        {% endif %}
        {% if _cdata[2]['TRX'] == "RX" %}
        <td style="font-weight:bold; padding-left: 20px; text-align:center;color:#464646"><b>M: {{_master}} </td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#0d1a81;">{{ _cdata[2]['SUB']|safe }} [<span style="align-items: center;justify-content:center;font-size: 8pt;font-weight:600;color:brown;">TS {{ 1 if _cdata[1]['TS'] == True else 2 }}</span>]</td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#b5651d;">{{ _cdata[2]['DEST']|safe }}</td>
        {% endif %}
    </tr>
    {% endif %}
    {% endfor %}
    {% endfor %}

{% else %}
         <table style='width:1100px; font: 13pt arial, sans-serif; margin-top:8px;'>
             <tr style='border:none; background-color:#f1f1f1;'>
             <td style='border:none;height:60px;'><font color=brown><b><center>Waiting for Data from FreeDMR Server ...</center></b></td>
             </tr>
            </table>
 {% endif %}
    {% for _peer, _pdata  in _table['PEERS'].items() %}
    {% if _pdata[1]['TS'] == True or _pdata[2]['TS'] == True %}
    <tr style="background-color:#f9f9f9f9;">
        {% if _pdata[1]['TRX'] == "RX" %}
        <td style="font-weight:bold; padding-left: 20px; text-align:center;color:#464646;">P: {{_peer}} </td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#0d1a81;">{{ _pdata[1]['SUB']|safe }} [<span style="align-items: center;justify-content:center;font-size: 8pt;font-weight:600;color:brown;">TS {{ 1 if _pdata[1]['TS'] == True else 2 }}</span>]</td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#b5651d;">{{ _cdata[1]['DEST']|safe }}</td>
        {% endif %}
        {% if _pdata[2]['TRX'] == "RX" %}
        <td style="font-weight:bold; padding-left: 20px; text-align:center;color:#464646;">P: {{_peer}} </td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#0d1a81;">{{ _pdata[2]['SUB']|safe }} [<span style="align-items: center;justify-content:center;font-size: 8pt;font-weight:600;color:brown;">TS {{ 1 if _pdata[1]['TS'] == True else 2 }}</span>]</td>
        <td style="font: 9.5pt arial, sans-serif;font-weight: 600;color:#b5651d;">{{ _pdata[2]['DEST']|safe }}</td>
        {% endif %}
    </tr>
    {% endif %}
    {% endfor %}
    <tr style="background-color:#f0f0f0;"><td colspan=3 height=5pt><hr style="height:1px;border:none;color:#f0f0f0;background-color:#f0f0f0;"></hr></td></tr>

{% if _table['OPENBRIDGES']|length >0 %}
    <tr style="background-color:#265b8a;" "height:30px;width:1100px; font: 10pt arial, sans-serif;{{ themec }}">
        <th>Systems OpenBridge</th>
        <th colspan=2 '>Active Incoming Calls</th>
    </tr>
    {% for _openbridge in _table['OPENBRIDGES'] %}
    {% set rx = namespace(value=0) %}
    {% if _table['OPENBRIDGES'][_openbridge]['STREAMS']|length >0 %}
       {% for entry in _table['OPENBRIDGES'][_openbridge]['STREAMS'] if _table['OPENBRIDGES'][_openbridge]['STREAMS'][entry][0]=='RX' %}
         {% set rx.value=1 %}
       {% endfor %}
       {% if rx.value == 1 %}    
       <tr style="background-color:#de8184;">
         <td style="font-weight:bold; padding-left: 20px; text-align:center;"> {{ _openbridge}} </td>
         <td colspan=2 style="background-color:#a1dcb5; font: 9pt arial, sans-serif; font-weight: 600; color:#464646;">
         {% for entry in _table['OPENBRIDGES'][_openbridge]['STREAMS']  if _table['OPENBRIDGES'][_openbridge]['STREAMS'][entry][0] == 'RX' %}[<span style="color:#008000;">{{ _table['OPENBRIDGES'][_openbridge]['STREAMS'][entry][0] }}</span>: <font color=#0065ff> {{ _table['OPENBRIDGES'][_openbridge]['STREAMS'][entry][1] }}</font> >> <font color=#b5651d> {{ _table['OPENBRIDGES'][_openbridge]['STREAMS'][entry][2] }}</font>]&nbsp; {% endfor %}
        </td>
      </tr>
      {% endif %}
   {% endif %}
   {% endfor %}
{% endif %}
</table>
</fieldset>






{% if _table['SETUP']['LASTHEARD'] == True %}
<fieldset class="big">
  <legend><b>.: Lastheard :.</b></legend>
  <table class="log">
    <tr>
      <th>Date</th>
      <th>Time</th>
      <th>Callsign (DMR-Id)</th>
      <th>Name</th>
      <th>TG#</th>
      <th>TG Name</th>
      <th>TX (s)</th>
      <th>System</th>
    </tr>
  {% for itm in lastheard %}
    <tr>
      <td>{{ itm[0][:10] }}</td>
      <td>{{ itm[0][11:] }}</td>
    {% if not itm[7] %}
      <td class="txt-464646"><b>{{ itm[6] }}</b></td>
      <td></td>
    {% else %}
      <td><a target="_blank" href=https://qrz.com/db/{{itm[7][0]}}>{{ itm[7][0] }}</a></b><span class="fnt-7pt">&nbsp;({{ itm[6] }})</span></td>
      <td <span style="color: #000000;"><b>{{ itm[7][1] }}</b></td>
    {% endif %}
      <td class="txt-b5651d"><b>{{ itm[4] }}</b></td>
      <td <span style="color: #454545;"><b>{{ '' if not itm[5] else itm[5]|safe }}</b></td>
      <td {{ 'class="bkgnd-1d1"'|safe if not itm[1] else '' }}>{{ 'DATA' if not itm[1] else itm[1]|int }}</td>
      <td>{{ itm[3] }}</td>
    </tr>
  {% endfor %}
  </table>
</fieldset>
{% endif %}

<fieldset class="big">
  <legend><b>.: Connected to Server :.</b></legend>
  <div class="conn2srv">
{% if _table['MASTERS']|length >0 %}
<tr style="background-color:#A7A2A2;"><td>
<br>
<div style="text-align:left;"><span style="color:#464646;font-weight:600;line-height:1.4;">&nbsp;&nbsp;LINKS:</span></div>
<div style="text-align:left;font:9.5pt arial, sans-serif;font-weight:bold;margin-left:25px; margin-right:25px;line-height:1.4;white-space:normal;">
    {% for _master in _table['MASTERS'] %}    
    {% if _table['MASTERS'][_master]['PEERS']|length >0 %}
    {% for _client, _cdata in _table['MASTERS'][_master]['PEERS'].items() %}
    <span class="tooltip" style="border-bottom: 0px dotted white;">
    <a style="border-bottom: 0px dotted white;font: 9.5pt arial,sans-serif;font-weight:bold;color:#0066ff;" target="_blank" href="http://www.qrz.com/db/{{_cdata['CALLSIGN']}}"><b>{{_cdata['CALLSIGN']}}</b></a>
    <span class="tooltiptext" style="left:115%;top:-10px;">
        <span style="font: 9pt arial,sans-serif;color:#3df8f8">
        &nbsp;&nbsp;&nbsp;<b>DMR ID</b>: <b><font color=yellow>{{ _client }}</b></font><br>
        {% if _cdata['RX_FREQ'] == 'N/A' and _cdata['TX_FREQ'] == 'N/A' %}
             &nbsp;&nbsp;&nbsp;<b>Type: <font color=yellow>IP Network</font></b><br>
        {% else %} 
            &nbsp;&nbsp;&nbsp;<b>Type: <font color=yellow>Radio</font></b> ({{ _cdata['SLOTS'] }})<br>
        {% endif %}
        &nbsp;&nbsp;&nbsp;<b>Hardware</b>: {{_cdata['PACKAGE_ID'] }}
        <br>&nbsp;&nbsp;&nbsp;<b>Soft_Ver</b>: {{_cdata['SOFTWARE_ID'] }}
        <br>&nbsp;&nbsp;&nbsp;<b>Info</b>: {{_cdata['LOCATION']}}
         <br>&nbsp;&nbsp;&nbsp;<b>Master</b>: <font color=yellow>{{_master}}</font>
        </span></span></span>&nbsp;
    {% endfor %}
    {% endif %}
    {% endfor %}
</div>
{% endif %}

        {% if _table['PEERS']|length >0 %}
        <h4 class="tittle">PEERS:</h4>
        <div class="hs-peers">
          {% for _peer, _pdata  in _table['PEERS'].items() %}
          <div class="tooltip" style="border-bottom: 1px dotted white;{{'background-color:#98FB98; color:#464646;' if _table['PEERS'][_peer]['STATS']['CONNECTION'] == 'YES' else 'background-color:#ff0000; color:white;'}}"><b>&nbsp;&nbsp;{{_peer}}&nbsp;&nbsp;</b>
            {% if _table['PEERS'][_peer]['STATS']['CONNECTION'] == 'YES' %}
            <span class="tooltiptext c2s-pos2">Connected</span>
            {% else %}
            <span class="tooltiptext c2s-pos2"><b>Disconnected</b></span>
            {% endif %}
          </div>
          {% endfor %}
        </div>
        {% endif %}
  </div>
</fieldset>


EOF
#
sed -i "s/1100/1200/g" /opt/FDMR-Monitor-B/templates/*.*
#sed -i "s/Copyright (c) 2016-.*/Copyright (c) <?php \$cdate=date(\"Y\"); if (\$cdate > \"2016\") {\$cdate=\"2016-\".date(\"Y\");} echo \$cdate; ?><br>/g" /opt/FDMR-Monitor-B/html/*.php
#sed -i "s/meta name=\"description.*/meta name=\"description\" content=\"Copyright (c) 2016-22.The Regents of the K0USY Group. All rights reserved. Version OA4DOA 2022 (v270422)\">/g" /opt/FDMR-Monitor-B/html/*.php
sed -i 's/b1eee9/3bb43d/' /opt/FDMR-Monitor-B/html/css/*.*
#sed -i "s/All rights reserved.<br>.*/All rights reserved.<br><a title=\"Raspbian Proyect by HP3ICC © <?php \$cdate=date(\"Y\"); if (\$cdate > \"2018\") {\$cdate=\"2018-\".date(\"Y\");} echo \$cdate; ?>\" target=\"_blank\" href=https:\/\/gitlab.com\/hp3icc\/fdmr-b\/>Script project: FDMR Bridge<\/a><br>/g" /opt/FDMR-Monitor-B/html/*.php
sed -i '166 s/hotpink/#ad02fd/g'   /opt/FDMR-Monitor-B/html/css/styles.php
sed -i '217 s/color:white/color:black/'  /opt/FDMR-Monitor-B/html/css/styles.php
#
cd /opt/FDMR-Monitor-B
if [ -f "mon.db" ]
then
   rm mon.db
fi
python3 mon_db.py --create
python3 mon_db.py --update
cp -r /opt/FDMR-Monitor-B/html/* /var/www/fdmr-b/ 
sudo chown www-data:www-data /var/www/fdmr-b/ -R    
sed -i "s/opt\/FDMR-Monitor/opt\/FDMR-Monitor-B/g"  /opt/FDMR-Monitor-B/utils/logrotate/fdmr_mon
mv /opt/FDMR-Monitor-B/utils/logrotate/fdmr_mon /etc/logrotate.d/fdmr_mon-b
systemctl stop apache2
systemctl disable apache2
#####################
chmod +x /opt/FDMR-Monitor-B/sysinfo/*
sh /opt/FDMR-Monitor-B/sysinfo/rrd-db.sh
##################
#Service
##########
sudo cat > /lib/systemd/system/proxy2.service  <<- "EOF"
[Unit]
Description= Proxy Service Bridge 
After=multi-user.target

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
ExecStart=/usr/bin/python3 /opt/FreeDMR-Bridge/hotspot_proxy_v2.py -c /opt/FreeDMR-Bridge/proxy.cfg
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
sudo cat > /lib/systemd/system/fdmr_mon-b.service  <<- "EOF"
[Unit]
Description=FDMR Monitor
# To make the network-online.target available
# systemctl enable systemd-networkd-wait-online.service
#After=network-online.target syslog.target
#Wants=network-online.target

[Service]
#User=root
#Type=simple
#Restart=always
#RestartSec=3
#StandardOutput=null
WorkingDirectory=/opt/FDMR-Monitor-B
ExecStart=python3 /opt/FDMR-Monitor-B/monitor.py
#Restart=on-abort
Restart=on-failure


[Install]
WantedBy=multi-user.target

EOF
#
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr)"
###
chmod +x /bin/menu-fdmr
ln -sf /bin/menu-fdmr /bin/MENU-FDMR
sh /opt/extra-1.sh
sh /opt/extra-2.sh
####################################################################
#monitor2
if [ ! -d "/var/www" ]
then
  mkdir -p /var/www
fi
if [ -d "/var/www/fdmr-b2" ]
then
  rm -r /var/www/fdmr-b2
fi
mkdir /var/www/fdmr-b2
if [ -d "/opt/FDMR-Monitor-B2" ]
then
  rm -r /opt/FDMR-Monitor-B2
fi
if ! grep -q "Web-Dashboar-name" "/opt/wdp7" > /dev/null 2>&1; then
cat > /opt/wdp7 <<- "EOFX"
################################################
# Select number port, FreeDMR-Bridge Dashboard #
################################################

Web-Dashboar-name: freedmr-bidge
Web-Dashboar-Port:  80
Web-Dashboar-dns:
Web-Dashboar-dir: /var/www/fdmr-b2
Web-Dashboar-select: fdmr_mon-b2
Web-Dashboar-soft: /opt/FDMR-Monitor-B2

EOFX
fi
cd /opt
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor2.git /opt/FDMR-Monitor-B2
#sudo git clone https://github.com/CS8ABG/FDMR-Monitor.git /opt/FDMR-Monitor-B2
cd /opt/FDMR-Monitor-B2
#sudo pip install -U -r requirements.txt
chmod 755 /opt/FDMR-Monitor-B2/data

#sed -i '72d' /opt/FDMR-Monitor-B2/html/include/navbar.php
#sed '69 a \                <option value="es">ES</option>' -i /opt/FDMR-Monitor-B2/html/include/navbar.php
sudo chmod 644 /opt/FDMR-Monitor-B/fdmr-mon_SAMPLE.cfg
find /opt/FDMR-Monitor-B2 -type f -exec sed -i 's/9000/9200/g' {} +
sed -i "s/4321/4382/g"  /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i 's/RELOAD_TIME =.*/RELOAD_TIME = 1/' /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i 's/FREQUENCY =.*/FREQUENCY = 1/' /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/LANGUAGE =.*/LANGUAGE = \"es\"/g"  /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/TELEGRAM/#TELEGRAM/g"  /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor-B2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor-B2/html/config.php
sed -i "s/PRIVATE_NETWORK = True/PRIVATE_NETWORK = False/g"  /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
variable2=$(date +'%Y' | tail -c 5)
#sed -i "s/CS8ABG<\/a> Dash/FDMR-Monitor2<\/a> by <a href=\"http:\/\/www.qrz.com\/db\/CS8ABG\"target=\"_blank\">CS8ABG<\/a>, Script project: <a title=\"Raspbian Proyect by HP3ICC © 2018-$variable2\" href=\"https:\/\/gitlab.com\/hp3icc\/fdmr-b\/\"target=\"_blank\">FDMR Bridge<\/a>/g" /opt/FDMR-Monitor-B2/html/include/footer.php
sed -i "s|PEER_URL =.*|PEER_URL = https://database.radioid.net/static/rptrs.json|g" /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/SUBSCRIBER_URL =.*/SUBSCRIBER_URL = http:\/\/datafiles.ddns.net:8888\/user.json/g" /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/TGID_URL =.*/TGID_URL = http:\/\/datafiles.ddns.net:8888\/talkgroup_ids.json/g" /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/FILES_PATH =.*/FILES_PATH = \/opt\/FDMR-Monitor2\/data/g" /opt/FDMR-Monitor-B2/fdmr-mon_SAMPLE.cfg
sed -i "s/#fff/#d1d1d1/g"  /opt/FDMR-Monitor-B2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/f8f9fa/d0d0d0/g"  /opt/FDMR-Monitor-B2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor-B2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor-B2/html/ssconfunc.php
#sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /var/www/fdmr2/ssconfunc.php

wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1cWqClH0FUxV5d-rVnibI_-iCypFnTDb3' -O /opt/FDMR-Monitor-B2/html/img/bk.jpg &&
wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1vsviKjpZmL_mWVEW2UstNODGPl8-uNVo' -O /opt/flags.zip &&

sudo unzip -o /opt/flags.zip -d /opt/FDMR-Monitor-B2/html/flags/ &&
rm /opt/flags.zip

cp fdmr-mon_SAMPLE.cfg fdmr-mon.cfg
sudo chmod 644 fdmr-mon.cfg
sudo cp /opt/FDMR-Monitor-B2/html/* /var/www/fdmr-b2/ -r

#*****
mv /opt/FDMR-Monitor-B2/utils/logrotate/fdmr_mon /opt/FDMR-Monitor-B2/utils/logrotate/fdmr_mon2
mv /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon.service /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon2.service
# \
sed -i "s/\/.*/\/opt\/FDMR-Monitor-B2\/log\/fdmr-mon.log {/g" /opt/FDMR-Monitor-B2/utils/logrotate/fdmr_mon2
sed -i "s/Description=.*/Description=FDMR Monitor-B2/g" /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon2.service
sed -i "s/WorkingDirectory=.*/WorkingDirectory=\/opt\/FDMR-Monitor-B2/g" /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon2.service
sed -i "s/ExecStart=.*/ExecStart=python3 \/opt\/FDMR-Monitor-B2\/monitor.py/g" /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon2.service
sudo cp /opt/FDMR-Monitor-B2/utils/logrotate/fdmr_mon2 /etc/logrotate.d/fdmr_mon-b2
sudo cp /opt/FDMR-Monitor-B2/utils/systemd/fdmr_mon2.service  /lib/systemd/system/fdmr_mon-b2.service
#sudo rm mon.db

if [ -f "mon.db" ]
then
   rm mon.db
fi

cd /opt/FDMR-Monitor-B2
cp -r /opt/FDMR-Monitor-B2/html/* /var/www/fdmr-b2/ 
sudo chown www-data:www-data /var/www/fdmr-b2/ -R 

#python3 mon_db.py --create
#python3 mon_db.py --update

#############################
sudo systemctl daemon-reload
# Registra el final en /opt/curl.txt
echo "Finalizado: $SCRIPT_NAME" >> /opt/curl.txt
