#!/bin/bash
# Verificar si el usuario tiene permisos de root
if [ $EUID -ne 0 ]; then
    whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
    exit 0
fi
if command -v apache2 >/dev/null 2>&1; then
    echo "Apache está instalado."

    # Verifica si Apache está activo (running)
    if systemctl is-active --quiet apache2; then
        echo "Apache está en ejecución. Deteniéndolo..."
        sudo systemctl stop apache2
    fi

    # Verifica si Apache está habilitado
    if systemctl is-enabled --quiet apache2; then
        echo "Apache está habilitado. Deshabilitándolo..."
        sudo systemctl disable apache2
    fi

    echo "Apache ha sido detenido y deshabilitado de forma permanente."
else
    echo "Apache no está instalado en el sistema."
fi
#######
cd /
######################################################################################################################
#                                                           Cronedit
######################################################################################################################
sudo cat > /usr/local/bin/cronedit.sh <<- "EOF"
#!/bin/bash
cronjob_editor () {
# usage: cronjob_editor '<interval>' '<command>' <add|remove>

if [[ -z "$1" ]] ;then printf " no interval specified\n" ;fi
if [[ -z "$2" ]] ;then printf " no command specified\n" ;fi
if [[ -z "$3" ]] ;then printf " no action specified\n" ;fi

if [[ "$3" == add ]] ;then
    # add cronjob, no duplication:
    ( sudo crontab -l | grep -v -F -w "$2" ; echo "$1 $2" ) | sudo crontab -
elif [[ "$3" == remove ]] ;then
    # remove cronjob:
    ( sudo crontab -l | grep -v -F -w "$2" ) | sudo crontab -
fi
}
cronjob_editor "$1" "$2" "$3"


EOF
sudo chmod +x /usr/local/bin/cronedit.sh

if [ -f "/var/www/html/tgcount.php" ];
then
   sudo rm /var/www/html/* -r
 #echo "found file"
else
 echo "file not found"
fi
if [ -f "/opt/FreeDMR/config/FreeDMR.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/FreeDMR/config/FreeDMR.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -f "/opt/adn-server/config/adn.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/adn-server/config/adn.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -f "/opt/ADN-DMR-Peer-Server/config/adn.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/ADN-DMR-Peer-Server/config/adn.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -z "$variable" ]
then variable=0000
fi

SERVICIOS="adn-server.service freedmr.service proxy.service adn-parrot.service fdmrparrot.service adn_server.service adn_proxy.service adn_parrot.service adn_dashboard"

for servicio in $SERVICIOS; do
    # Verificar si el servicio existe
    if systemctl list-unit-files --type=service | grep -q "^$servicio "; then
        echo "El servicio $servicio existe."

        # Verificar si el servicio está activo
        if systemctl is-active --quiet $servicio; then
            echo "El servicio $servicio está activo. Deteniendo..."
            sudo systemctl stop $servicio
        else
            echo "El servicio $servicio no está activo."
        fi

        # Deshabilitar el servicio
        echo "Deshabilitando el servicio $servicio..."
        sudo systemctl disable $servicio
    else
        echo "El servicio $servicio no existe."
    fi
done

if [ -f "/lib/systemd/system/adn-parrot.service" ];
then
  rm /lib/systemd/system/adn-parrot.service
fi
if [ -f "/lib/systemd/system/adn_parrot.service" ];
then
  rm /lib/systemd/system/adn_parrot.service
fi
if [ -f "/lib/systemd/system/fdmrparrot.service" ];
then
  rm /lib/systemd/system/fdmrparrot.service
fi
if [ -f "/lib/systemd/system/freedmr.service" ];
then
  rm /lib/systemd/system/freedmr.service
fi
if [ -f "/opt/FreeDMR/config/FreeDMR.cfg" ];
then
  mv /opt/FreeDMR/config/FreeDMR.cfg /opt/Old_Config-FreeDMR.cfg
fi
if [ -d "/opt/FreeDMR" ];
then
  rm -r /opt/FreeDMR/
fi
##########################################################

if [ -d "/var/log/ADN" ];then
   sudo rm -r /var/log/ADN 2>/dev/null
fi
if [ -d "/opt/ADN-DMR-Peer-Server" ];then
   sudo rm -r /opt/ADN-DMR-Peer-Server 2>/dev/null
fi
if [ -d "/var/www/fdmr2" ];then
   sudo rm -r /var/www/fdmr2 2>/dev/null
fi
if [ -d "/opt/FDMR-Monitor2" ];then
   sudo rm -r /opt/FDMR-Monitor2 2>/dev/null
fi
if [ -d "/var/www/fdmr" ];then
   sudo rm -r /var/www/fdmr 2>/dev/null
fi
if [ -d "/opt/FDMR-Monitor" ];then
   sudo rm -r /opt/FDMR-Monitor 2>/dev/null
fi
###############################
if [ -f "/bin/menu-fdmr" ];then
   rm  /bin/menu-fdmr
 #echo "found file"
else
 echo "file not found"

fi
########################
if [ ! -d "/var/www" ]
then
  mkdir -p /var/www
fi
if [ ! -d "/var/www/fdmr" ]
then
  mkdir -p /var/www/fdmr
fi
if [ ! -d "/var/www/fdmr2" ]
then
  mkdir -p /var/www/fdmr2
fi
if [ ! -d "/var/log/ADN" ]
then
  mkdir -p /var/log/ADN
fi
########################
if [ -f "/opt/obp.txt" ]
then
   echo "found file"
else
  sudo cat > /opt/obp.txt <<- "EOF"
 
#Coloque abajo su lista de obp y peer
EOF
fi
##################################
if [ -f "/opt/extra-1.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-1.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
if [ -f "/opt/extra-2.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-2.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
if [ -f "/opt/extra-3.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-3.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
sudo chmod +x /opt/extra-*
#########################
#lamp
#"libapache2-mod-php"
apps="mariadb-server php php-zip php-mbstring php-cli php-common php-curl php-xml php-mysql"

# Función para verificar e instalar una aplicación
check_and_install() {
    app=$1
    if ! dpkg -s $app 2>/dev/null | grep -q "Status: install ok installed"; then
        echo "$app no está instalado. Instalando..."
        sudo apt-get install -y $app
        echo "$app instalado correctamente."
    else
        echo "$app ya está instalado."
    fi
}

# Verificar e instalar cada aplicación
for app in $apps; do
    check_and_install $app
done
sudo systemctl restart mariadb &&
sudo systemctl enable mariadb
#sudo mysql_secure_installation  --host=localhost --port=3306
#echo "DROP USER emqte1@localhost" | /usr/bin/mysql -u root
#echo "DROP DATABASE selfcare" | /usr/bin/mysql -u root

newUser='hbmon'
newDbPassword='hbmon'
newDb='hbmon'
host=localhost
#host='%'

# MySQL 5.7 and earlier versions
#commands="CREATE DATABASE \`${newDb}\`;CREATE USER '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT ALL privileges ON \`${newDb}\`.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';FLUSH PRIVILEGES;"

# MySQL 8 and higher versions
commands="CREATE DATABASE IF NOT EXISTS \`${newDb}\`;CREATE USER IF NOT EXISTS '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}';GRANT ALL ON \`${newDb}\`.* TO '${newUser}'@'${host}';FLUSH PRIVILEGES;"

#cho "${commands}" | /usr/bin/mysql -u root -p
echo "${commands}" | /usr/bin/mysql -u root
#################
#FDMR Server
#sudo -H pip3 install --upgrade pip
#pip install pyopenssl --upgrade
cd /opt
git clone https://github.com/Amateur-Digital-Network/ADN-DMR-Peer-Server.git
cd ADN-DMR-Peer-Server
sudo git checkout develop
if [ ! -d "/var/log/ADN" ]; then
  mkdir /var/log/ADN
fi
if [ ! -d "/opt/ADN-DMR-Peer-Server/data" ]; then
  mkdir /opt/ADN-DMR-Peer-Server/data
fi
if [ "$(ls -A "/opt/ADN-DMR-Peer-Server/data/")" ]; then
  rm -rf "/opt/ADN-DMR-Peer-Server/data/"*
fi
sudo chmod -R +x /opt/ADN-DMR-Peer-Server/*
sudo chmod -R 777 /opt/ADN-DMR-Peer-Server/*
#./install.sh
cp /opt/ADN-DMR-Peer-Server/config/ADN-SAMPLE.cfg /opt/ADN-DMR-Peer-Server/config/adn.cfg
sed -i "s/SERVER_ID:.*/SERVER_ID: $variable/g"  /opt/ADN-DMR-Peer-Server/config/adn.cfg
cat /opt/obp.txt >> /opt/ADN-DMR-Peer-Server/config/adn.cfg
#
cd /opt/ADN-DMR-Peer-Server/
#############
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/hotspot_proxy_v2.py -O /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/proxy_SAMPLE.cfg -O /opt/ADN-DMR-Peer-Server/proxy.cfg
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/proxy_db.py -O /opt/ADN-DMR-Peer-Server/proxy_db.py
####################
sed -i "s/db_username =.*/db_username = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_password =.*/db_password = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_name =.*/db_name = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/\/freedmr.cfg/\/config\/adn.cfg/g" /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_selfcare =.*/db_selfcare = ProxyDB('localhost', 'hbmon', 'hbmon', 'hbmon', 3306)/g" /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/USERNAME =.*/USERNAME = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/PASSWORD =.*/PASSWORD = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/DB_NAME =.*/DB_NAME = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54000/56400/g"  /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/54000/56400/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54100/56499/g"  /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/54100/56499/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54000/56400/g" /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/54100/56499/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/1234567/1234567,1231237,123123701/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
#sed -i '228s/20/50/' /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
#######################
#FDMR-Monitor
cd /opt
#sudo git clone https://github.com/yuvelq/FDMR-Monitor.git
#sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor.git
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor.git
cd FDMR-Monitor
sudo git checkout Self_Service
#
sudo chmod +x install.sh
chmod -R 777 /opt/FDMR-Monitor/*
chmod -R +x /opt/FDMR-Monitor/*
#
sudo chmod 644 /opt/FDMR-Monitor/fdmr-mon_SAMPLE.cfg
####
sudo chmod +x /opt/FDMR-Monitor/sysinfo/cpu.sh
sudo chmod +x /opt/FDMR-Monitor/sysinfo/graph.sh
sudo chmod +x /opt/FDMR-Monitor/sysinfo/rrd-db.sh
#
cd /opt/FDMR-Monitor/
#sudo rm /opt/FDMR-Monitor/install.sh
################
apps=("rrdtool")

for app in "${apps[@]}"
do
    # Verificar apps
    if ! dpkg -s "$app" >/dev/null 2>&1; then
        # app no instalada
        sudo apt-get install -y "$app"
    else
        # app ya instalada
        echo "$app ya instalada"
    fi
done
sed -i "s/www\/html/www\/fdmr/g" /opt/FDMR-Monitor/html/*.*
sed -i "s/www\/html/www\/fdmr/g" /opt/FDMR-Monitor/sysinfo/*.*
# Install the required support programs
#sudo pip install -U -r requirements.txt
cd /opt/FDMR-Monitor/
cp /opt/FDMR-Monitor/fdmr-mon_SAMPLE.cfg /opt/FDMR-Monitor/fdmr-mon.cfg
###############################
#wget -O /opt/FDMR-Monitor/html/img/logo.png https://adn.systems/files/logo1.png 
#wget -O /opt/FDMR-Monitor/html/favicon.ico https://adn.systems/files/ADN_37x18_01.ico 
#
sudo cat > /opt/FDMR-Monitor/html/buttons.php <<- "EOF"
<!-- HBMonitor buttons HTML code -->
<a class="button" href="index.php">Home</a>
&nbsp;
<div class="dropdown">
  <button class="dropbtn">Links</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="linkedsys.php">Linked Systems</a>

<a class="button" href="statictg.php">Static TG</a>

<a class="button" href="opb.php">OpenBridge</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">Self Service</button>
  <div class="dropdown-content">
    <?php if(!PRIVATE_NETWORK){echo '<a class="button" href="selfservice.php">SelfService</a>';}?>
    <a class="button" href="login.php">Login</a>
    <?php 
    if(isset($_SESSION["auth"], $_SESSION["callsign"], $_SESSION["h_psswd"]) and $_SESSION["auth"]){
      echo '<a class="button" href="devices.php">Devices</a>';
    }
    ?>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Local Server</button>
  <div class="dropdown-content">

<a class="button" href="moni.php">&nbsp;Monitor&nbsp;</a>

<a class="button" href="sysinfo.php">&nbsp;System Info&nbsp;</a>

<a class="button" href="log.php">&nbsp;Lastheard&nbsp;</a>

<a class="button" href="tgcount.php">&nbsp;TG Count&nbsp;</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">ADN-Systems</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="https://adn.systems/"target="_blank">&nbsp;Global Info ADN Systems;</a>
<a class="button" href="https://adn.systems/servers-list/"target="_blank">&nbsp;Info Server&nbsp;</a>
<a class="button" href="https://t.me/ADN_Systems"target="_blank">&nbsp;World Wide Telegram Groups&nbsp;</a>
&nbsp;
</div>
</div>

<!--
<a class="button" href="bridges.php">Bridges</a>
-->
<!-- Example of buttons dropdown HTML code -->
<!--
<div class="dropdown">
  <button class="dropbtn">Admin Area</button>
  <div class="dropdown-content">
    <a href="masters.php">Master&Peer</a>
    <a href="opb.php">OpenBridge</a>
    <a href="moni.php">Monitor</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Reflectors</button>
  <div class="dropdown-content">
    <a target='_blank' href="#">YSF Reflector</a>
    <a target='_blank' href="#">XLX950</a>
  </div>
</div>
-->
EOF

#
cp -r /opt/FDMR-Monitor/html/* /var/www/fdmr/ 
sudo chown www-data:www-data /var/www/fdmr/ -R    
cp /opt/FDMR-Monitor/utils/logrotate/fdmr_mon /etc/logrotate.d/
#
#sed -i "s/THEME_COLOR =.*/THEME_COLOR = pro/g" /opt/FDMR-Monitor/fdmr-mon.cfg
if [ -f "mon.db" ]
then
   rm mon.db
fi
cd /opt/FDMR-Monitor
python3 mon_db.py --create
python3 mon_db.py --update

systemctl stop apache2
systemctl disable apache2
#####################
chmod +x /opt/FDMR-Monitor/sysinfo/*
sh /opt/FDMR-Monitor/sysinfo/rrd-db.sh
(crontab -l; echo "*/5 * * * * sh /opt/FDMR-Monitor/sysinfo/graph.sh")|awk '!x[$0]++'|crontab -
(crontab -l; echo "*/2 * * * * sh /opt/FDMR-Monitor/sysinfo/cpu.sh")|awk '!x[$0]++'|crontab -
##################
#Service
sudo cat > /lib/systemd/system/proxy.service <<- "EOF"
[Unit]
Description= Proxy Service 
After=multi-user.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py -c /opt/ADN-DMR-Peer-Server/proxy.cfg
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#########
bash -c "$(curl -fsSLk https://raw.githubusercontent.com/hp3icc/D-APRS/main/emq-daprs.sh)"
#########
sudo cat > /lib/systemd/system/adn-server.service <<- "EOF"
[Unit]
Description=FreeDmr
After=multi-user.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/bridge_master.py -c /opt/ADN-DMR-Peer-Server/config/adn.cfg
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
###
servicios="adn-parrot.service adn_parrot.service fdmrparrot.service"

# Recorre la lista de servicios y deshabilita y detiene sin enviar mensaje
for servicio in $servicios; do
    # Verifica si el servicio existe antes de intentar deshabilitarlo y detenerlo
    if systemctl list-unit-files --type=service | grep -q "^$servicio"; then
        sudo systemctl disable --quiet "$servicio"
        sudo systemctl stop --quiet "$servicio"
    fi
    
    # Verifica si el archivo del servicio existe antes de intentar eliminarlo
    if [ -f /lib/systemd/system/"$servicio" ]; then
        sudo rm /lib/systemd/system/"$servicio"
    fi
done
sudo cat > /lib/systemd/system/adn-parrot.service <<- "EOF"
[Unit]
Description=Freedmr Parrot
After=network-online.target syslog.target
Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/ADN-DMR-Peer-Server
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/playback.py -c /opt/ADN-DMR-Peer-Server/config/parrot.cfg


[Install]
WantedBy=multi-user.target

EOF
#
sudo cat > /lib/systemd/system/fdmr_mon.service  <<- "EOF"
[Unit]
Description=FDMR Monitor
# To make the network-online.target available
# systemctl enable systemd-networkd-wait-online.service
#After=network-online.target syslog.target
#Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/FDMR-Monitor
ExecStart=python3 /opt/FDMR-Monitor/monitor.py
#Restart=on-abort
#Restart=on-failure


[Install]
WantedBy=multi-user.target

EOF
#
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr)"
###
chmod +x /bin/menu-fdmr
ln -sf /bin/menu-fdmr /bin/MENU-FDMR
sh /opt/extra-1.sh
sh /opt/extra-2.sh
####################################################################
#monitor2
cd /opt
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor2.git
#sudo git clone https://github.com/CS8ABG/FDMR-Monitor.git /opt/FDMR-Monitor2
cd /opt/FDMR-Monitor2
sudo git checkout Self_Service
#sudo pip install -U -r requirements.txt
if [ ! -d "/opt/FDMR-Monitor2/data" ]; then
  mkdir /opt/FDMR-Monitor2/data
fi
if [ "$(ls -A "/opt/FDMR-Monitor2/data/")" ]; then
  rm -rf "/opt/FDMR-Monitor2/data/"*
fi
sudo chmod -R 777 /opt/FDMR-Monitor2/*
sudo chmod -R +x /opt/FDMR-Monitor2/*
sudo chmod 644 /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i 's/RELOAD_TIME =.*/RELOAD_TIME = 1/' /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i 's/FREQUENCY =.*/FREQUENCY = 1/' /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/LANGUAGE =.*/LANGUAGE = \"es\"/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_NAME =.*/DB_NAME = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_USERNAME =.*/DB_USERNAME = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_PASSWORD =.*/DB_PASSWORD = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/TELEGRAM/#TELEGRAM/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor2/html/config.php
sed -i "s/PRIVATE_NETWORK = True/PRIVATE_NETWORK = False/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
variable2=$(date +'%Y' | tail -c 5)
#sed -i "s/CS8ABG<\/a> Dash/FDMR-Monitor2<\/a> by <a href=\"http:\/\/www.qrz.com\/db\/CS8ABG\"target=\"_blank\">CS8ABG<\/a>, <a href=https:\/\/gitlab.hacknix.net\/hacknix\/FreeDMR\/-\/wikis\/Home>FreeDMR<\/a> by <a href=\"http:\/\/www.qrz.com\/db\/G7RZU\"target=\"_blank\">G7RZU hacknix<\/a>, Script project: <a title=\"Script proyect by HP3ICC © 2018-$variable2\" href=\"https:\/\/gitlab.com\/hp3icc\/fdmr\/\"target=\"_blank\">FDMR+<\/a>/g" /opt/FDMR-Monitor2/html/include/footer.php
sed -i "s|SUBSCRIBER_URL.*|SUBSCRIBER_URL = https://adn.systems/files/subscriber_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s|TGID_URL.*|TGID_URL = https://adn.systems/files/talkgroup_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s|PEER_URL.*|PEER_URL = https://adn.systems/files/peer_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/FILES_PATH =.*/FILES_PATH = \/opt\/FDMR-Monitor2\/data/g" /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/#fff/#d1d1d1/g"  /opt/FDMR-Monitor2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/f8f9fa/d0d0d0/g"  /opt/FDMR-Monitor2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor2/html/ssconfunc.php
#sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /var/www/fdmr2/ssconfunc.php
##########################################################
#wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1cWqClH0FUxV5d-rVnibI_-iCypFnTDb3' -O /opt/FDMR-Monitor2/html/img/bk.jpg &&
#wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1vsviKjpZmL_mWVEW2UstNODGPl8-uNVo' -O /opt/flags.zip &&
#sudo unzip -o /opt/flags.zip -d /opt/FDMR-Monitor2/html/flags/ &&
#rm /opt/flags.zip
#wget -O /opt/FDMR-Monitor2/html/img/logo.png https://adn.systems/files/logo1.png 
#wget -O /opt/FDMR-Monitor2/html/img/favicon.ico https://adn.systems/files/ADN_37x18_01.ico 
#wget -O /opt/FDMR-Monitor2/html/img/Logo_mini.png https://adn.systems/files/Logo_mini.png
cp fdmr-mon_SAMPLE.cfg fdmr-mon.cfg
sudo chmod 644 fdmr-mon.cfg
sudo cp /opt/FDMR-Monitor2/html/* /var/www/fdmr2/ -r

#*****
mv /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2
mv /opt/FDMR-Monitor2/utils/systemd/fdmr_mon.service /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
# \
sed -i "s/\/.*/\/opt\/FDMR-Monitor2\/log\/fdmr-mon.log {/g" /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2
sed -i "s/Description=.*/Description=FDMR Monitor2/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
sed -i "s/WorkingDirectory=.*/WorkingDirectory=\/opt\/FDMR-Monitor2/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
sed -i "s/ExecStart=.*/ExecStart=python3 \/opt\/FDMR-Monitor2\/monitor.py/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service

sudo cp /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2 /etc/logrotate.d/fdmr_mon2

#sudo cp /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service /lib/systemd/system/fdmr_mon2.service
#sudo rm mon.db
##############################################################
#                 service update
############################################################

cat > /lib/systemd/system/fdmr_mon2.service  <<- "EOF"
[Unit]
Description=FDMR Monitor2
# To make the network-online.target available
# systemctl enable systemd-networkd-wait-online.service
#After=network-online.target syslog.target
#Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/FDMR-Monitor2
ExecStart=python3 /opt/FDMR-Monitor2/monitor.py
#Restart=on-abort
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#
if systemctl status mariadb.service |grep inactive >/dev/null 2>&1
then sudo systemctl start mariadb.service

fi
if systemctl status mariadb.service |grep disable >/dev/null 2>&1
then sudo systemctl enable mariadb.service

fi
if [ -f "mon.db" ]
then
   rm mon.db
fi

cd /opt/FDMR-Monitor2

#python3 mon_db.py --create
#python3 mon_db.py --update

#############################
sudo systemctl daemon-reload

