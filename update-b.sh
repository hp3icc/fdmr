#!/bin/bash

# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
#whiptail --title "FreeDMR Bridge - Update" --msgbox "scrip sin actualizaciones disponibles / scrip no updates available" 0 50
SERVICIOS="http.server-fdmr-b.service http.server-fdmr-b2.service"

for servicio in $SERVICIOS; do
    # Verificar si el servicio existe
    if systemctl list-unit-files --type=service | grep -q "^$servicio "; then
        echo "El servicio $servicio existe."

        # Verificar si el servicio está activo
        if systemctl is-active --quiet $servicio; then
            echo "El servicio $servicio está activo. Deteniendo..."
            sudo systemctl stop $servicio
        else
            echo "El servicio $servicio no está activo."
        fi

        # Deshabilitar el servicio
        echo "Deshabilitando el servicio $servicio..."
        sudo systemctl disable $servicio
    else
        echo "El servicio $servicio no existe."
    fi
done
##############
#!/bin/bash
if ! grep -q "Web-Dashboar-name" "/opt/wdp7" > /dev/null 2>&1; then
   sudo cat > /opt/wdp7 <<- "EOFX"
################################################
# Select number port, FreeDMR-Bridge Dashboard #
################################################

Web-Dashboar-name: freedmr-bidge
Web-Dashboar-Port:  80
Web-Dashboar-dns:
Web-Dashboar-dir: /var/www/fdmr-b2
Web-Dashboar-select: fdmr_mon-b2
Web-Dashboar-soft: /opt/FDMR-Monitor-B2

EOFX
fi
if [ ! -f "/etc/nginx/sites-available/000" ]; then
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/nginx.sh)"
fi
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr-bridge)"
whiptail --title "FreeDMR Bridge - Update" --msgbox "scrip sin actualizaciones disponibles / scrip no updates available" 0 50
