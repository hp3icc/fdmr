#!/bin/bash
# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
(crontab -l | grep -v "sync ; echo 3 > /proc/sys/vm/drop_caches >/dev/null 2>&1") | crontab -
##############
cd /
##################
#!/bin/bash
if ! grep -q "Web-Dashboar-name" "/opt/wdp" > /dev/null 2>&1; then
cat > /opt/wdp <<- "EOFX"
############################################
# Select number port, ADN-Server Dashboard #
############################################

Web-Dashboar-name: adn
Web-Dashboar-Port:  80
Web-Dashboar-dns:
Web-Dashboar-dir: /var/www/fdmr2
Web-Dashboar-select: fdmr_mon2
Web-Dashboar-soft: /opt/FDMR-Monitor2
 
EOFX
fi
if ! grep -q "Web-Dashboar-name: adn" "/opt/wdp" > /dev/null 2>&1; then
   sed -i "s|Web-Dashboar-name:.*|Web-Dashboar-name: adn|g" /opt/wdp
fi
if [ ! -f "/etc/nginx/sites-available/000" ]; then
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/nginx.sh)"
fi
if [ -f "/opt/extra-1.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-1.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
if [ -f "/opt/extra-2.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-2.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
if [ -f "/opt/extra-3.sh" ]
then
  echo "found file"
else
  sudo cat > /opt/extra-3.sh <<- "EOF"
#!/bin/bash
######################################################################
# Coloque en este archivo, cualquier instruccion shell adicional que # 
# quierre se realice al finalizar la actualizacion.                  #
######################################################################
 
  
EOF
# 
fi
sudo chmod +x /opt/extra-*
cd /
########
sudo cat > /bin/menu-update <<- "EOF"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

while : ; do
choix=$(whiptail --title "Script proyect HP3ICC / update FDMR+" --menu "Nota Importante: antes de iniciar la actualizacion, el proceso de actualizacion borrara por completo todas las configuraciones, al finalizar la actualizacion el servicio se reinniciara automaticamente.
" 17 50 4 \
1 " Update ADN-Server " \
2 " Update FDMR-Monitor OA4DOA " \
3 " Update FDMR-Monitor CS8ABG " \
4 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
menu-up-adn ;;
2)
menu-up-fdmon ;;
3)
menu-up-fdmon2 ;;
4)
break;
esac
done
exit 0


EOF
#

sudo cat > /bin/menu-up-adn <<- "EOF"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

while : ; do
choix=$(whiptail --title "Script proyect HP3ICC / update ADN-Server" --menu "Nota Importante: debe debe agregar todos sus obp en la opcion numero uno, ( 1-Lista de OBP )antes de iniciar la actualizacion, el proceso de actualizacion borrara por completo la carpeta /opt/ADN-DMR-Peer-Server, al finalizar la actualizacion el servicio Freedmr se reinniciara automaticamente.
" 18 55 5 \
1 " List OBP " \
2 " shell extra " \
3 " Iniciar Actualizacion ADN-Server  " \
4 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo nano /opt/obp.txt ;;
2)
sudo nano /opt/extra-1.sh && chmod +x /opt/extra* ;;
3)
sudo /opt/fdmr-update.sh ;;
4)
break;
esac
done
exit 0

EOF
#
sudo cat > /bin/menu-up-fdmon <<- "EOF"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

while : ; do
choix=$(whiptail --title "Script proyect HP3ICC / update FDMR-Monitor OA4DOA" --menu "Nota Importante: antes de iniciar la actualizacion, el proceso de actualizacion borrara por completo todas las configuraciones, al finalizar la actualizacion el servicio se reinniciara automaticamente.
" 17 50 4 \
1 " shell extra " \
2 " Iniciar Actualizacion FDMR-Monitor OA4DOA " \
3 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo nano /opt/extra-2.sh && chmod +x /opt/extra* ;;
2)
sudo /opt/monitor-update.sh ;;
3)
break;
esac
done
exit 0


EOF
sudo cat > /bin/menu-up-fdmon2 <<- "EOF"
#!/bin/bash
if [[ $EUID -ne 0 ]]; then
        whiptail --title "sudo su" --msgbox "requiere ser usuario root , escriba (sudo su) antes de entrar a menu / requires root user, type (sudo su) before entering menu" 0 50
        exit 0
fi

while : ; do
choix=$(whiptail --title "Script proyect HP3ICC / update FDMR-Monitor CS8ABG" --menu "Nota Importante: antes de iniciar la actualizacion, el proceso de actualizacion borrara por completo todas las configuraciones, al finalizar la actualizacion el servicio se reinniciara automaticamente.
" 17 50 4 \
1 " shell extra " \
2 " Iniciar Actualizacion FDMR-Monitor CS8ABG" \
3 " Menu Principal " 3>&1 1>&2 2>&3)
exitstatus=$?
#on recupere ce choix
#exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Your chosen option:" $choix
else
    echo "You chose cancel."; break;
fi
# case : action en fonction du choix
case $choix in
1)
sudo nano /opt/extra-3.sh && chmod +x /opt/extra* ;;
2)
sudo /opt/monitor-update2.sh ;;
3)
break;
esac
done
exit 0


EOF

######################################### ADN-Server Update  ###############################################################
sudo cat > /opt/fdmr-update.sh <<- "EOFD1"
#!/bin/bash
#"libapache2-mod-php"
apps="mariadb-server php php-zip php-mbstring php-cli php-common php-curl php-xml php-mysql"

# Función para verificar e instalar una aplicación
check_and_install() {
    app=$1
    if ! dpkg -s $app 2>/dev/null | grep -q "Status: install ok installed"; then
        echo "$app no está instalado. Instalando..."
        sudo apt-get install -y $app
        echo "$app instalado correctamente."
    else
        echo "$app ya está instalado."
    fi
}

# Verificar e instalar cada aplicación
for app in $apps; do
    check_and_install $app
done
sudo systemctl restart mariadb &&
sudo systemctl enable mariadb
#sudo mysql_secure_installation  --host=localhost --port=3306
#echo "DROP USER emqte1@localhost" | /usr/bin/mysql -u root
#echo "DROP DATABASE selfcare" | /usr/bin/mysql -u root

newUser='hbmon'
newDbPassword='hbmon'
newDb='hbmon'
host=localhost
#host='%'

# MySQL 5.7 and earlier versions
#commands="CREATE DATABASE \`${newDb}\`;CREATE USER '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT ALL privileges ON \`${newDb}\`.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';FLUSH PRIVILEGES;"

# MySQL 8 and higher versions
commands="CREATE DATABASE IF NOT EXISTS \`${newDb}\`;CREATE USER IF NOT EXISTS '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}';GRANT ALL ON \`${newDb}\`.* TO '${newUser}'@'${host}';FLUSH PRIVILEGES;"

#cho "${commands}" | /usr/bin/mysql -u root -p
echo "${commands}" | /usr/bin/mysql -u root

###############
cd /
if [ -f "/opt/FreeDMR/config/FreeDMR.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/FreeDMR/config/FreeDMR.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -f "/opt/adn-server/config/adn.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/adn-server/config/adn.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -f "/opt/ADN-DMR-Peer-Server/config/adn.cfg" ]
then
   variable=$(grep "SERVER_ID:" /opt/ADN-DMR-Peer-Server/config/adn.cfg | grep -Eo '[0-9]{1,9}')
fi
if [ -z "$variable" ]
then variable=0000

fi
SERVICIOS="adn-server.service freedmr.service proxy.service adn-parrot.service fdmrparrot.service adn_server.service adn_proxy.service adn_parrot.service"

for servicio in $SERVICIOS; do
    # Verificar si el servicio existe
    if systemctl list-unit-files --type=service | grep -q "^$servicio "; then
        echo "El servicio $servicio existe."

        # Verificar si el servicio está activo
        if systemctl is-active --quiet $servicio; then
            echo "El servicio $servicio está activo. Deteniendo..."
            sudo systemctl stop $servicio
        else
            echo "El servicio $servicio no está activo."
        fi

        # Deshabilitar el servicio
        echo "Deshabilitando el servicio $servicio..."
        sudo systemctl disable $servicio
    else
        echo "El servicio $servicio no existe."
    fi
done

if [ -f "/lib/systemd/system/fdmrparrot.service" ];
then
  rm /lib/systemd/system/fdmrparrot.service
fi
if [ -f "/lib/systemd/system/freedmr.service" ];
then
  rm /lib/systemd/system/freedmr.service
fi
if [ -f "/opt/FreeDMR/config/FreeDMR.cfg" ];
then
  mv /opt/FreeDMR/config/FreeDMR.cfg /opt/Old_Config-FreeDMR.cfg
fi
if [ -d "/opt/FreeDMR" ];
then
  rm -r /opt/FreeDMR/
fi
#########################
#Service
sudo cat > /lib/systemd/system/proxy.service <<- "EOF"
[Unit]
Description= Proxy Service 
After=multi-user.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py -c /opt/ADN-DMR-Peer-Server/proxy.cfg
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#########
sudo cat > /lib/systemd/system/adn-server.service <<- "EOF"
[Unit]
Description=FreeDmr
After=multi-user.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/bridge_master.py -c /opt/ADN-DMR-Peer-Server/config/adn.cfg
#Restart=on-failure


[Install]
WantedBy=multi-user.target

EOF
###
sudo cat > /lib/systemd/system/adn-parrot.service <<- "EOF"
[Unit]
Description=Freedmr Parrot
After=network-online.target syslog.target
Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/ADN-DMR-Peer-Server
ExecStart=/usr/bin/python3 /opt/ADN-DMR-Peer-Server/playback.py -c /opt/ADN-DMR-Peer-Server/config/parrot.cfg
#Restart=on-abort
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#
if [ -d "/opt/ADN-DMR-Peer-Server" ]
then
   sudo rm -r /opt/ADN-DMR-Peer-Server
fi
if [ -d "/var/log/FreeDMR" ]
then
   sudo rm -r /var/log/FreeDMR
fi
#
#sudo -H pip3 install --upgrade pip
#pip install pyopenssl --upgrade
cd /opt
git clone https://github.com/Amateur-Digital-Network/ADN-DMR-Peer-Server.git
cd ADN-DMR-Peer-Server
sudo git checkout develop
if [ ! -d "/var/log/ADN" ]; then
  mkdir /var/log/ADN
fi
if [ ! -d "/opt/ADN-DMR-Peer-Server/data" ]; then
  mkdir /opt/ADN-DMR-Peer-Server/data
fi
if [ "$(ls -A "/opt/ADN-DMR-Peer-Server/data/")" ]; then
  rm -rf "/opt/ADN-DMR-Peer-Server/data/"*
fi
sudo chmod -R +x /opt/ADN-DMR-Peer-Server/*
sudo chmod -R 777 /opt/ADN-DMR-Peer-Server/*
if [ -f "/etc/os-release" ]; then
    source /etc/os-release

    if [ "$VERSION_ID" == "12" ]; then
        /usr/bin/python3 -m pip install --break-system-packages -r requirements.txt
    else
        /usr/bin/python3 -m pip install --upgrade -r requirements.txt
    fi
fi
####
cp /opt/ADN-DMR-Peer-Server/config/ADN-SAMPLE.cfg  /opt/ADN-DMR-Peer-Server/config/adn.cfg
sudo sed -i "s/SERVER_ID:.*/SERVER_ID: $variable/g"  /opt/ADN-DMR-Peer-Server/config/adn.cfg
sudo cat /opt/obp.txt >> /opt/ADN-DMR-Peer-Server/config/adn.cfg

cd /opt/ADN-DMR-Peer-Server/

#############
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/hotspot_proxy_v2.py -O /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/proxy_SAMPLE.cfg -O /opt/ADN-DMR-Peer-Server/proxy.cfg
wget https://raw.githubusercontent.com/CS8ABG/FDMR-Monitor/Self_Service/proxy/proxy_db.py -O /opt/ADN-DMR-Peer-Server/proxy_db.py
####################
sed -i "s/db_username =.*/db_username = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_password =.*/db_password = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_name =.*/db_name = 'hbmon'/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/\/freedmr.cfg/\/config\/adn.cfg/g" /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/db_selfcare =.*/db_selfcare = ProxyDB('localhost', 'hbmon', 'hbmon', 'hbmon', 3306)/g" /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/USERNAME =.*/USERNAME = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/PASSWORD =.*/PASSWORD = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/DB_NAME =.*/DB_NAME = hbmon/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54000/56400/g"  /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/54000/56400/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54100/56499/g"  /opt/ADN-DMR-Peer-Server/proxy_db.py
sed -i "s/54100/56499/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
sed -i "s/54000/56400/g" /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/54100/56499/g"  /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
sed -i "s/1234567/1234567,1231237,123123701/g" /opt/ADN-DMR-Peer-Server/proxy.cfg
#sed -i '228s/20/50/' /opt/ADN-DMR-Peer-Server/hotspot_proxy_v2.py
#################
# Verificar y actualizar python3-venv si no está instalado
if ! dpkg -s python3-venv >/dev/null 2>&1; then
    echo "python3-venv no está instalado. Instalando..."
    apt-get install python3-venv -y
    echo "python3-venv instalado correctamente."
fi

# Crear y activar un entorno virtual
cd /opt/
python3 -m venv myenv
# Detectar el sistema operativo y su versión
if [ -f "/etc/os-release" ]; then
    . /etc/os-release
    OS=$ID
    VERSION=$VERSION_ID
else
    echo "No se pudo detectar el sistema operativo."
    exit 1
fi
# Instalación de módulos pip según el sistema operativo
if [ "$OS" == "debian" ] && [ "$VERSION" == "12" ]; then
    source myenv/bin/activate
    /usr/bin/python3 -m pip uninstall --break-system-packages twisted -y
    /usr/bin/python3 -m pip install --break-system-packages twisted==22.10.0
    deactivate
else
    sudo pip3 uninstall -y twisted --quiet
    pip install Twisted==22.10.0
fi
sh /opt/extra-1.sh
sudo systemctl daemon-reload
sudo systemctl start proxy.service
sudo systemctl start adn-server.service
sudo systemctl start adn-parrot.service
sudo systemctl enable proxy.service
sudo systemctl enable adn-server.service
sudo systemctl enable adn-parrot.service
if systemctl status fdmr_mon.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon.service
   sudo systemctl start fdmr_mon.service

fi
if systemctl status fdmr_mon2.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon2.service
   sudo systemctl start fdmr_mon2.service

fi
######
bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu-fdmr)"
if [ -d "/opt/D-APRS" ]
then
   echo "found file"
else
  bash -c "$(curl -fsSLk https://raw.githubusercontent.com/hp3icc/D-APRS/main/emq-daprs.sh)"
fi

EOFD1
sudo chmod +x /opt/fdmr-update.sh
######################################### FDMR-Monitor Update  ###############################################################
sudo cat > /opt/monitor-update.sh <<- "EOFB1"
#!/bin/bash

# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
if ! grep -q "Web-Dashboar-name: adn" "/opt/wdp" > /dev/null 2>&1; then
   sed -i "s|Web-Dashboar-name:.*|Web-Dashboar-name: adn|g" /opt/wdp
fi
##############

cd /opt
variable1=$(grep "REPORT_NAME =" /opt/FDMR-Monitor/fdmr-mon.cfg)
variable2=$(grep "THEME_COLOR =" /opt/FDMR-Monitor/fdmr-mon.cfg)
variable3=$(grep "COLOR_TEXT =" /opt/FDMR-Monitor/fdmr-mon.cfg)
variable4=$(grep "COLOR_1 =" /opt/FDMR-Monitor/fdmr-mon.cfg)
variable5=$(grep "COLOR_2 =" /opt/FDMR-Monitor/fdmr-mon.cfg)

#####################################
SERVICIOS="adn_dashboard.service apache2.service"

for servicio in $SERVICIOS; do
    # Verificar si el servicio existe
    if systemctl list-unit-files --type=service | grep -q "^$servicio "; then
        echo "El servicio $servicio existe."

        # Verificar si el servicio está activo
        if systemctl is-active --quiet $servicio; then
            echo "El servicio $servicio está activo. Deteniendo..."
            sudo systemctl stop $servicio
            sudo systemctl disable $servicio
        else
            echo "El servicio $servicio no está activo."
        fi
    else
        echo "El servicio $servicio no existe."
    fi
done
sudo systemctl stop fdmr_mon2.service
sudo systemctl stop fdmr_mon.service

##############################################################
#                 service update
############################################################

cat > /lib/systemd/system/fdmr_mon.service  <<- "EOF"
[Unit]
Description=FDMR Monitor
# To make the network-online.target available
# systemctl enable systemd-networkd-wait-online.service
#After=network-online.target syslog.target
#Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/FDMR-Monitor
ExecStart=python3 /opt/FDMR-Monitor/monitor.py
#Restart=on-abort
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#
if [ -d "/var/www/fdmr" ];then
   sudo rm -r /var/www/fdmr 2>/dev/null
fi
if [ -d "/opt/FDMR-Monitor" ];then
   sudo rm -r /opt/FDMR-Monitor 2>/dev/null
fi
#########################
#lamp

systemctl restart mariadb
systemctl enable mariadb
#sudo mysql_secure_installation  --host=localhost --port=3306
#echo "DROP USER emqte1@localhost" | /usr/bin/mysql -u root
#echo "DROP DATABASE selfcare" | /usr/bin/mysql -u root

newUser='hbmon'
newDbPassword='hbmon'
newDb='hbmon'
host=localhost
#host='%'

# MySQL 5.7 and earlier versions
#commands="CREATE DATABASE \`${newDb}\`;CREATE USER '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT ALL privileges ON \`${newDb}\`.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';FLUSH PRIVILEGES;"

# MySQL 8 and higher versions
commands="CREATE DATABASE IF NOT EXISTS \`${newDb}\`;CREATE USER IF NOT EXISTS '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}';GRANT ALL ON \`${newDb}\`.* TO '${newUser}'@'${host}';FLUSH PRIVILEGES;"

#cho "${commands}" | /usr/bin/mysql -u root -p
echo "${commands}" | /usr/bin/mysql -u root

DATABASE_NAME="selfcare"

# Comando para verificar si la base de datos existe
CHECK_DB=$(mysql -u root -e "SHOW DATABASES LIKE '$DATABASE_NAME'")

# Comprobar el resultado
if [ "$CHECK_DB" ]; then
#  echo "La base de datos $DATABASE_NAME se encontró."
  mysqldump -u root selfcare > hbmon.sql
  mysql -u root hbmon < hbmon.sql
  mysql -u root -e "DROP DATABASE IF EXISTS selfcare;"
  rm hbmon.sql
fi
###FDMR-Monitor
cd /opt
#sudo git clone https://github.com/yuvelq/FDMR-Monitor.git
#sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor.git
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor.git
cd FDMR-Monitor
sudo git checkout Self_Service
sudo chmod +x install.sh
chmod -R 777 /opt/FDMR-Monitor/*
chmod -R +x /opt/FDMR-Monitor/*
########
sudo apt-get install rrdtool -y
sudo sed -i "s/www\/html/www\/fdmr/g" /opt/FDMR-Monitor/html/*.*
sudo sed -i "s/www\/html/www\/fdmr/g" /opt/FDMR-Monitor/sysinfo/*.*

sudo chmod +x /opt/FDMR-Monitor/sysinfo/cpu.sh
sudo chmod +x /opt/FDMR-Monitor/sysinfo/graph.sh
sudo chmod +x /opt/FDMR-Monitor/sysinfo/rrd-db.sh

cd /opt/FDMR-Monitor/
#sudo rm /opt/FDMR-Monitor/install.sh
################
# Install the required support programs
#sudo pip install -U -r requirements.txt
cd /opt/FDMR-Monitor/
cp /opt/FDMR-Monitor/fdmr-mon_SAMPLE.cfg /opt/FDMR-Monitor/fdmr-mon.cfg
###############################
#wget -O /opt/FDMR-Monitor/html/img/logo.png https://adn.systems/files/logo1.png 
#wget -O /opt/FDMR-Monitor/html/favicon.ico https://adn.systems/files/ADN_37x18_01.ico 
#
sudo cat > /opt/FDMR-Monitor/html/buttons.php <<- "EOF"
<!-- HBMonitor buttons HTML code -->
<a class="button" href="index.php">Home</a>
&nbsp;
<div class="dropdown">
  <button class="dropbtn">Links</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="linkedsys.php">Linked Systems</a>

<a class="button" href="statictg.php">Static TG</a>

<a class="button" href="opb.php">OpenBridge</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">Self Service</button>
  <div class="dropdown-content">
    <?php if(!PRIVATE_NETWORK){echo '<a class="button" href="selfservice.php">SelfService</a>';}?>
    <a class="button" href="login.php">Login</a>
    <?php 
    if(isset($_SESSION["auth"], $_SESSION["callsign"], $_SESSION["h_psswd"]) and $_SESSION["auth"]){
      echo '<a class="button" href="devices.php">Devices</a>';
    }
    ?>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Local Server</button>
  <div class="dropdown-content">

<a class="button" href="moni.php">&nbsp;Monitor&nbsp;</a>

<a class="button" href="sysinfo.php">&nbsp;System Info&nbsp;</a>

<a class="button" href="log.php">&nbsp;Lastheard&nbsp;</a>

<a class="button" href="tgcount.php">&nbsp;TG Count&nbsp;</a>
&nbsp;
</div>
</div>
<div class="dropdown">
  <button class="dropbtn">ADN-Systems</button>
  <div class="dropdown-content">
&nbsp;
<a class="button" href="https://adn.systems/"target="_blank">&nbsp;Global Info ADN Systems;</a>
<a class="button" href="https://adn.systems/servers-list/"target="_blank">&nbsp;Info Server&nbsp;</a>
<a class="button" href="https://t.me/ADN_Systems"target="_blank">&nbsp;World Wide Telegram Groups&nbsp;</a>
&nbsp;
</div>
</div>

<!--
<a class="button" href="bridges.php">Bridges</a>
-->
<!-- Example of buttons dropdown HTML code -->
<!--
<div class="dropdown">
  <button class="dropbtn">Admin Area</button>
  <div class="dropdown-content">
    <a href="masters.php">Master&Peer</a>
    <a href="opb.php">OpenBridge</a>
    <a href="moni.php">Monitor</a>
  </div>
</div>
<div class="dropdown">
  <button class="dropbtn">Reflectors</button>
  <div class="dropdown-content">
    <a target='_blank' href="#">YSF Reflector</a>
    <a target='_blank' href="#">XLX950</a>
  </div>
</div>
-->
EOF

#
mkdir /var/www/fdmr/ 
cp -r /opt/FDMR-Monitor/html/* /var/www/fdmr/ 
sudo chown www-data:www-data /var/www/fdmr/ -R    
cp /opt/FDMR-Monitor/utils/logrotate/fdmr_mon /etc/logrotate.d/
#
if [ -f "mon.db" ]
then
   rm mon.db
fi

mysql -u root -e "DROP TABLE IF EXISTS hbmon.last_heard;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.lstheard_log;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.peer_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.subscriber_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.talkgroup_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.tg_count;"
cd /opt/FDMR-Monitor
python3 mon_db.py --create &&
python3 mon_db.py --update


systemctl stop apache2
systemctl disable apache2
#####################
sudo sed -i "s/REPORT_NAME =.*/$variable1/g" /opt/FDMR-Monitor/fdmr-mon.cfg
sudo sed -i "s/THEME_COLOR =.*/$variable2/g" /opt/FDMR-Monitor/fdmr-mon.cfg
sudo sed -i "s/COLOR_TEXT =.*/$variable3/g" /opt/FDMR-Monitor/fdmr-mon.cfg
sudo sed -i "s/COLOR_1 =.*/$variable4/g" /opt/FDMR-Monitor/fdmr-mon.cfg
sudo sed -i "s/COLOR_2 =.*/$variable5/g" /opt/FDMR-Monitor/fdmr-mon.cfg

chmod +x /opt/FDMR-Monitor/sysinfo/*
sh /opt/FDMR-Monitor/sysinfo/rrd-db.sh

systemctl daemon-reload &&

##############
if systemctl status mariadb.service |grep inactive >/dev/null 2>&1
then sudo systemctl start mariadb.service

fi
if systemctl status mariadb.service |grep disable >/dev/null 2>&1
then sudo systemctl enable mariadb.service

fi
if systemctl status fdmr_mon.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon.service
   sudo systemctl start fdmr_mon.service
   (crontab -l; echo "* */24 * * * rm /opt/FDMR-Monitor/data/*") | awk '!x[$0]++' | crontab -
   (crontab -l; echo "*/2 * * * * sh /opt/FDMR-Monitor/sysinfo/cpu.sh") | awk '!x[$0]++' | crontab -
   (crontab -l; echo "*/5 * * * * sh /opt/FDMR-Monitor/sysinfo/graph.sh") | awk '!x[$0]++' | crontab -
   (crontab -l | grep -v "rm /opt/FDMR-Monitor2/data/*") | crontab -

fi
if systemctl status fdmr_mon2.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon2.service
   sudo systemctl start fdmr_mon2.service
   (crontab -l; echo "* */24 * * * rm /opt/FDMR-Monitor2/data/*") | awk '!x[$0]++' | crontab -
   (crontab -l | grep -v "rm /opt/FDMR-Monitor/data/*") | crontab -
   (crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/cpu.sh") | crontab -
   (crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/graph.sh") | crontab -

fi
##############
   sh /opt/extra-2.sh

if [ -f "/etc/nginx/sites-enabled/freedmr" ]; then
  mv /etc/nginx/sites-enabled/freedmr /etc/nginx/sites-enabled/adn
fi
if ! systemctl status nginx.service |grep "service; enabled;" >/dev/null 2>&1
then 
   systemctl enable nginx.service
fi
sudo systemctl restart nginx

EOFB1
sudo chmod +x /opt/monitor-update.sh
######################################### FDMR-Monitor2 Update  ###############################################################
sudo cat > /opt/monitor-update2.sh <<- "EOFB2"
#!/bin/bash
# Verificar si el usuario tiene permisos de root
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ejecutarse como usuario ROOT"
    exit 1
fi
if ! grep -q "Web-Dashboar-name: adn" "/opt/wdp" > /dev/null 2>&1; then
   sed -i "s|Web-Dashboar-name:.*|Web-Dashboar-name: adn|g" /opt/wdp
fi
##############
SERVICIOS="adn_dashboard.service apache2.service"

for servicio in $SERVICIOS; do
    # Verificar si el servicio existe
    if systemctl list-unit-files --type=service | grep -q "^$servicio "; then
        echo "El servicio $servicio existe."

        # Verificar si el servicio está activo
        if systemctl is-active --quiet $servicio; then
            echo "El servicio $servicio está activo. Deteniendo..."
            sudo systemctl stop $servicio
        else
            echo "El servicio $servicio no está activo."
        fi

        # Deshabilitar el servicio
        echo "Deshabilitando el servicio $servicio..."
        sudo systemctl disable $servicio
    else
        echo "El servicio $servicio no existe."
    fi
done
sudo systemctl stop fdmr_mon.service
sudo systemctl stop fdmr_mon2.service

##############################################################
#                 service update
############################################################

cd /
if [ -d "/var/www/fdmr2" ]; then
    sudo rm -r /var/www/fdmr2 2>/dev/null
fi
if [ -d "/opt/FDMR-Monitor2" ];then
   sudo rm -r /opt/FDMR-Monitor2 2>/dev/null
fi
if [ ! -d "/var/www" ]
then
   mkdir -p /var/www
fi
if [ ! -d "/var/www/fdmr2" ]
then
   mkdir -p /var/www/fdmr2
fi
#########################
#lamp
systemctl restart mariadb
systemctl enable mariadb
#sudo mysql_secure_installation  --host=localhost --port=3306
#echo "DROP USER emqte1@localhost" | /usr/bin/mysql -u root
#echo "DROP DATABASE selfcare" | /usr/bin/mysql -u root

newUser='hbmon'
newDbPassword='hbmon'
newDb='hbmon'
host=localhost
#host='%'

# MySQL 5.7 and earlier versions
#commands="CREATE DATABASE \`${newDb}\`;CREATE USER '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT ALL privileges ON \`${newDb}\`.* TO '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';FLUSH PRIVILEGES;"

# MySQL 8 and higher versions
commands="CREATE DATABASE IF NOT EXISTS \`${newDb}\`;CREATE USER IF NOT EXISTS '${newUser}'@'${host}' IDENTIFIED BY '${newDbPassword}';GRANT USAGE ON *.* TO '${newUser}'@'${host}';GRANT ALL ON \`${newDb}\`.* TO '${newUser}'@'${host}';FLUSH PRIVILEGES;"

#cho "${commands}" | /usr/bin/mysql -u root -p
echo "${commands}" | /usr/bin/mysql -u root

DATABASE_NAME="selfcare"

# Comando para verificar si la base de datos existe
CHECK_DB=$(mysql -u root -e "SHOW DATABASES LIKE '$DATABASE_NAME'")

# Comprobar el resultado
if [ "$CHECK_DB" ]; then
#  echo "La base de datos $DATABASE_NAME se encontró."
  mysqldump -u root selfcare > hbmon.sql
  mysql -u root hbmon < hbmon.sql
  mysql -u root -e "DROP DATABASE IF EXISTS selfcare;"
  rm hbmon.sql
fi
#################
cd /opt
sudo git clone https://gitlab.com/hp3icc/FDMR-Monitor2.git
#sudo git clone https://github.com/CS8ABG/FDMR-Monitor.git /opt/FDMR-Monitor2
cd /opt/FDMR-Monitor2
sudo git checkout Self_Service
#sudo pip install -U -r requirements.txt
if [ ! -d "/opt/FDMR-Monitor2/data" ]; then
  mkdir /opt/FDMR-Monitor2/data
fi
if [ "$(ls -A "/opt/FDMR-Monitor2/data/")" ]; then
  rm -rf "/opt/FDMR-Monitor2/data/"*
fi
chmod -R 777 /opt/FDMR-Monitor2/*
chmod -R +x /opt/FDMR-Monitor2/*

sudo chmod 644 /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s|SUBSCRIBER_URL.*|SUBSCRIBER_URL = https://adn.systems/files/subscriber_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s|TGID_URL.*|TGID_URL = https://adn.systems/files/talkgroup_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s|PEER_URL.*|PEER_URL = https://adn.systems/files/peer_ids.json|g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i 's/RELOAD_TIME =.*/RELOAD_TIME = 1/' /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i 's/FREQUENCY =.*/FREQUENCY = 1/' /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/LANGUAGE =.*/LANGUAGE = \"es\"/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/TELEGRAM/#TELEGRAM/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_NAME =.*/DB_NAME = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_USERNAME =.*/DB_USERNAME = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/DB_PASSWORD =.*/DB_PASSWORD = hbmon/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/PRIVATE_NETWORK = True/PRIVATE_NETWORK = False/g"  /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/SUBSCRIBER_FILE =.*/SUBSCRIBER_FILE = subscriber_ids.json/g" /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/FILES_PATH =.*/FILES_PATH = \/opt\/FDMR-Monitor2\/data/g" /opt/FDMR-Monitor2/fdmr-mon_SAMPLE.cfg
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor2/html/config.php
variable2=$(date +'%Y' | tail -c 5)
sed -i "s/#fff/#d1d1d1/g"  /opt/FDMR-Monitor2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/f8f9fa/d0d0d0/g"  /opt/FDMR-Monitor2/html/plugins/adminlte/css/adminlte.min.css
sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /opt/FDMR-Monitor2/html/ssconfunc.php
#sed -i "s/configFile =.*/configFile = '\/opt\/FDMR-Monitor2\/fdmr-mon.cfg';/g" /var/www/fdmr2/ssconfunc.php

#wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1cWqClH0FUxV5d-rVnibI_-iCypFnTDb3' -O /opt/FDMR-Monitor2/html/img/bk.jpg &&
#wget -q --no-check-certificate -r 'https://docs.google.com/uc?export=download&id=1vsviKjpZmL_mWVEW2UstNODGPl8-uNVo' -O /opt/flags.zip &&
#sudo unzip -o /opt/flags.zip -d /opt/FDMR-Monitor2/html/flags/ &&
#rm /opt/flags.zip
#wget -O /opt/FDMR-Monitor2/html/img/logo.png https://adn.systems/files/logo1.png 
#wget -O /opt/FDMR-Monitor2/html/img/favicon.ico https://adn.systems/files/ADN_37x18_01.ico 
#wget -O /opt/FDMR-Monitor2/html/img/Logo_mini.png https://adn.systems/files/Logo_mini.png

sudo cp fdmr-mon_SAMPLE.cfg fdmr-mon.cfg
sudo chmod 644 fdmr-mon.cfg
sudo cp /opt/FDMR-Monitor2/html/* /var/www/fdmr2/ -r

###############################

mv /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2
mv /opt/FDMR-Monitor2/utils/systemd/fdmr_mon.service /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
# \
sed -i "s/\/.*/\/opt\/FDMR-Monitor2\/log\/fdmr-mon.log {/g" /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2
sed -i "s/Description=.*/Description=FDMR Monitor2/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
sed -i "s/WorkingDirectory=.*/WorkingDirectory=\/opt\/FDMR-Monitor2/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service
sed -i "s/ExecStart=.*/ExecStart=python3 \/opt\/FDMR-Monitor2\/monitor.py/g" /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service

sudo cp /opt/FDMR-Monitor2/utils/logrotate/fdmr_mon2 /etc/logrotate.d/fdmr_mon2

#sudo cp /opt/FDMR-Monitor2/utils/systemd/fdmr_mon2.service /lib/systemd/system/fdmr_mon2.service
##############################################################
#                 service update
############################################################

cat > /lib/systemd/system/fdmr_mon2.service  <<- "EOF"
[Unit]
Description=FDMR Monitor2
# To make the network-online.target available
# systemctl enable systemd-networkd-wait-online.service
#After=network-online.target syslog.target
#Wants=network-online.target

[Service]
User=root
Type=simple
Restart=always
RestartSec=3
StandardOutput=null
WorkingDirectory=/opt/FDMR-Monitor2
ExecStart=python3 /opt/FDMR-Monitor2/monitor.py
#Restart=on-abort
#Restart=on-failure

[Install]
WantedBy=multi-user.target

EOF
#
systemctl daemon-reload
if systemctl status mariadb.service |grep inactive >/dev/null 2>&1
then sudo systemctl start mariadb.service

fi
if systemctl status mariadb.service |grep disable >/dev/null 2>&1
then sudo systemctl enable mariadb.service

fi
if [ -f "mon.db" ]
then
   rm mon.db
fi

mysql -u root -e "DROP TABLE IF EXISTS hbmon.last_heard;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.lstheard_log;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.peer_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.subscriber_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.talkgroup_ids;" &&
mysql -u root -e "DROP TABLE IF EXISTS hbmon.tg_count;"
cd /opt/FDMR-Monitor2
python3 mon_db.py --create &&
python3 mon_db.py --update

sudo systemctl daemon-reload &&
#####################################
if systemctl status mariadb.service |grep inactive >/dev/null 2>&1
then sudo systemctl start mariadb.service

fi
if systemctl status mariadb.service |grep disable >/dev/null 2>&1
then sudo systemctl enable mariadb.service

fi
if systemctl status fdmr_mon.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon.service
   sudo systemctl start fdmr_mon.service
   (crontab -l; echo "* */24 * * * rm /opt/FDMR-Monitor/data/*") | awk '!x[$0]++' | crontab -
   (crontab -l; echo "*/2 * * * * sh /opt/FDMR-Monitor/sysinfo/cpu.sh") | awk '!x[$0]++' | crontab -
   (crontab -l; echo "*/5 * * * * sh /opt/FDMR-Monitor/sysinfo/graph.sh") | awk '!x[$0]++' | crontab -
   (crontab -l | grep -v "rm /opt/FDMR-Monitor2/data/*") | crontab -

fi
if systemctl status fdmr_mon2.service |grep "service; enabled;" >/dev/null 2>&1
then 
   sudo systemctl stop fdmr_mon2.service
   sudo systemctl start fdmr_mon2.service
   (crontab -l; echo "* */24 * * * rm /opt/FDMR-Monitor2/data/*") | awk '!x[$0]++' | crontab -
   (crontab -l | grep -v "rm /opt/FDMR-Monitor/data/*") | crontab -
   (crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/cpu.sh") | crontab -
   (crontab -l | grep -v "sh /opt/FDMR-Monitor/sysinfo/graph.sh") | crontab -

fi
sh /opt/extra-3.sh

if [ -f "/etc/nginx/sites-enabled/freedmr" ]; then
  mv /etc/nginx/sites-enabled/freedmr /etc/nginx/sites-enabled/adn
fi
if ! systemctl status nginx.service |grep "service; enabled;" >/dev/null 2>&1
then 
   systemctl enable nginx.service
fi
sudo systemctl restart nginx

EOFB2
sudo chmod +x /opt/monitor-update2.sh
######################################################################################################################
#                                                           Cronedit
######################################################################################################################
sudo cat > /usr/local/bin/cronedit.sh <<- "EOFC1"
#!/bin/bash
cronjob_editor () {
# usage: cronjob_editor '<interval>' '<command>' <add|remove>

if [[ -z "$1" ]] ;then printf " no interval specified\n" ;fi
if [[ -z "$2" ]] ;then printf " no command specified\n" ;fi
if [[ -z "$3" ]] ;then printf " no action specified\n" ;fi

if [[ "$3" == add ]] ;then
    # add cronjob, no duplication:
    ( sudo crontab -l | grep -v -F -w "$2" ; echo "$1 $2" ) | sudo crontab -
elif [[ "$3" == remove ]] ;then
    # remove cronjob:
    ( sudo crontab -l | grep -v -F -w "$2" ) | sudo crontab -
fi
}
cronjob_editor "$1" "$2" "$3"


EOFC1
sudo chmod +x /usr/local/bin/cronedit.sh

##########################################  End Update Files   ##############################################################
sudo systemctl daemon-reload

sudo chmod +x /opt/fdmr-update.sh
sudo chmod +x /opt/monitor-update.sh
sudo chmod +x /bin/menu*
sudo chmod +x /opt/monitor-update2.sh

bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/install/ipv6off.sh)" &&
if [ -d "/opt/ADN-DMR-Peer-Server/" ]; then
   menu-update
else
   bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/fdmr/-/raw/main/install.sh)"
   sudo systemctl daemon-reload
   sudo systemctl start proxy.service
   sudo systemctl start adn-server.service
   sudo systemctl start adn-parrot.service
   sudo systemctl enable proxy.service
   sudo systemctl enable adn-server.service
   sudo systemctl enable adn-parrot.service

   if [ -f "/bin/menu" ]; then
      bash -c "$(curl -fsSLk https://gitlab.com/hp3icc/emq-TE1/-/raw/main/menu/menu)"
   fi

   # Mensaje en español e inglés, separados por una línea en blanco
   mensaje="El software de su servidor ha sido actualizado a la versión ADN-DMR-Peer-Server. Por favor, salga por completo del menú e ingrese a:    menu-adn

   The software on your server has been updated to version ADN-DMR-Peer-Server. Please exit the menu completely and enter:    menu-adn"

   # Usar whiptail para mostrar el mensaje
   whiptail --title "Actualización de Software / Software Update" --msgbox "$mensaje" 15 60
fi
